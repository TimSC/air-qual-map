import msgpack
import gzip
from matplotlib import pyplot as plt
import pyproj
import csv
import datetime
import json
import numpy as np
import scipy.optimize as opt

def FilterWithinBbox(withNearPos, bbox):
	inside = {}
	for ts in withNearPos:
		pos = withNearPos[ts][0]
		measurement = withNearPos[ts][1]
		if pos[1] < bbox[0] or pos[1] > bbox[2]: continue
		if pos[0] < bbox[1] or pos[0] > bbox[3]: continue
		inside[ts] = withNearPos[ts]
	return inside

def FilterWithinDistance(withNearPos, pos, dist, invert=False):
	#pos is lon, lat in degrees

	geod = pyproj.Geod("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")

	out = {}
	for ts in withNearPos:
		mpos = withNearPos[ts][0]

		azimuth1, azimuth2, distance = geod.inv(pos[0], pos[1], mpos[1], mpos[0])
		if not invert and distance <= dist:
			out[ts] = withNearPos[ts]
		elif invert and distance > dist:
			out[ts] = withNearPos[ts]
	return out

def FilterNearestToPos(withNearPos, pos):

	geod = pyproj.Geod("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")

	out = {}
	bestDist = None
	for ts in withNearPos:
		mpos = withNearPos[ts][0]

		azimuth1, azimuth2, distance = geod.inv(pos[0], pos[1], mpos[1], mpos[0])
		if bestDist is None or distance	<= bestDist:
			out = {ts: withNearPos[ts]}
			bestDist = distance
	return bestDist, out

def GetWithinTimeRange(withNearPos, startDt, endDt):
	
	out = {}
	for ts in withNearPos:
		dt = datetime.datetime.utcfromtimestamp(ts)
		if startDt > dt: continue
		if endDt < dt: continue
		out[ts] = withNearPos[ts]

	return out

def GetSingleReading(withNearPos, measureName="NO2 (ppb)"): #pm 10 (ug/m3) , pm 2.5 (ug/m3), NO2 (ppb)

	out = []
	for ts in withNearPos:
		measurement = withNearPos[ts][1]
		out.append(measurement[measureName])
	return out		

def GetTimeOfDay(withNearPos):
	out = []
	for ts in withNearPos:
		dt = datetime.datetime.utcfromtimestamp(ts)
		val = 3600 * dt.hour + 60 * dt.minute + dt.second + dt.microsecond / 1000.0
		out.append(val)
	return out

def GetPosFromSeries(withNearPos):

	outLat, outLon = [], []
	for ts in withNearPos:
		pos = withNearPos[ts][0]
		outLat.append(pos[0])
		outLon.append(pos[1])

	return outLat, outLon

def FindRelevantHours(withNearPos):
	relevantHours = set()
	relevantHoursDict = {}
	for ts in withNearPos:
		dt = datetime.datetime.utcfromtimestamp(ts)
		before = datetime.datetime(dt.year, dt.month, dt.day, dt.hour)
		after = before+datetime.timedelta(hours=1)
		relevantHours.add(before)
		relevantHours.add(after)
		
		if dt.minute < 30:
			nearest = before
		else:
			nearest = after
		relevantHoursDict[ts] = nearest

	relevantHours = list(relevantHours)
	relevantHours.sort()
	return relevantHours, relevantHoursDict

def FindRelevantPos(withNearPos):
	geod = pyproj.Geod("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs")
	relevantPos = set()
	relevantPosDict = {}

	data = csv.DictReader(open("portsmouth/measuredno22019.csv", "rt"))
	monitoringPos = {}
	for li in data:
		siteId = li['Site ID']
		pos = (float(li['lon']), float(li['lat']))
		if len(li['2018']) == 0: continue #Invalid data
		no2 = float(li['2018'])
		latLon = (pos[1], pos[0])

		matchFound = False
		for existingPos in relevantPos:
			azimuth1, azimuth2, distance = geod.inv(existingPos[1], existingPos[0], pos[0], pos[1])
			if distance < 25.0:
				matchFound = True
				nearest = existingPos
				break

		if not matchFound:
			relevantPos.add(latLon)
			if latLon in monitoringPos:
				monitoringPos[latLon].append(no2)
			else:
				monitoringPos[latLon] = [no2]

	print ("relevantPos from monitoring", len(relevantPos))

	for ts in withNearPos:
		#Check if pos within threshold already exists
		mpos = tuple(withNearPos[ts][0])

		matchFound = False
		for existingPos in relevantPos:
			azimuth1, azimuth2, distance = geod.inv(existingPos[1], existingPos[0], mpos[1], mpos[0])
			if distance < 25.0:
				matchFound = True
				nearest = existingPos
				break
		#print (mpos, matchFound, len(relevantPos))
		if not matchFound:
			relevantPos.add(mpos)
			relevantPosDict[ts] = mpos
		else:
			relevantPosDict[ts] = nearest

	return list(relevantPos), relevantPosDict, monitoringPos

def EvalModel(x, *args, **kwargs):

	withNearPos, monitoringPos, linearM, linearC, no2vals = args
	ppbToUgm3 = 1.9125 #https://uk-air.defra.gov.uk/assets/documents/reports/cat06/0502160851_Conversion_Factors_Between_ppb_and.pdf

	out = []
	for ts in withNearPos:

		pos = tuple(relevantPosDict[ts])
		hour = relevantHoursDict[ts]

		no2reading = withNearPos[ts][1]['NO2 (ppb)'] * ppbToUgm3
		if pos in monitoringPos:
			err = x[linearM[hour]] * no2reading + x[linearC[hour]] - monitoringPos[pos]
		else:
			err = x[linearM[hour]] * no2reading + x[linearC[hour]] - x[no2vals[pos]]

		out.append(err)

	#print (sum(out))
	return out

if __name__=="__main__":

	print ("Reading measurements with position")
	fi = gzip.open("measurespos.dat.gz", "rb")
	withNearPos = msgpack.unpackb(fi.read(), strict_map_key=False)
	fi.close()
	print ("measured positions", len(withNearPos))

	#Consider data in the local area only
	bbox = (-1.1418915,50.758613,-0.9949493,50.8597104) #Portsmouth
	withNearPos = FilterWithinBbox(withNearPos, bbox)
	print ("local measurements", len(withNearPos))

	#Remove blacksites from data
	blackSiteList = json.load(open("blacksites.json", "rt"))
	for pos in blackSiteList:
		print ("remove black site", pos)
		withNearPos = FilterWithinDistance(withNearPos, pos[:2], pos[2], invert=True)
	print ("remaining measurements", len(withNearPos))

	if 0:
		print ("Reading positions")
		fi = gzip.open("positions.dat.gz", "rb")
		positions = msgpack.unpackb(fi.read(), strict_map_key=False)
		fi.close()

	#maxTs = max(withNearPos.keys())
	#print ("max ts", maxTs, datetime.datetime.fromtimestamp(maxTs))

	#bbox = (-1.063689,50.7908907,-1.0632974,50.7911789)
	#inside = FilterWithinBbox(withNearPos, bbox)
	#inside = GetWithinTimeRange(withNearPos, 
	#	datetime.datetime(2020, 5, 27),
	#	datetime.datetime(2020, 5, 28))

	#vals = GetSingleReading(inside)
	#timeOfDay = GetTimeOfDay(inside)

	#plt.plot(timeOfDay, vals, '.')
	#plt.show()

	#outLat, outLon = GetPosFromSeries(inside)
	#outLat, outLon = zip(*inside.values())
	#plt.plot(outLon, outLat, '.-')

	relevantHours, relevantHoursDict = FindRelevantHours(withNearPos)
	print ("relevant hours", len(relevantHours))

	if 0:
		relevantPos, relevantPosDict, monitoringPos = FindRelevantPos(withNearPos)
		
		fi = gzip.open("relevantPos.dat.gz", "wb")
		fi.write(msgpack.packb((relevantPos, relevantPosDict, 
			list(monitoringPos.keys()), list(monitoringPos.values()))))
		fi.close()
		
	else:
		fi = gzip.open("relevantPos.dat.gz", "rb")
		relevantPos, relevantPosDict, monitoringPosKeys, monitoringPosValues = msgpack.unpackb(fi.read(), strict_map_key=False)
		monitoringPos = dict(zip([tuple(k) for k in monitoringPosKeys], monitoringPosValues))
		relevantPos = [tuple(pos) for pos in relevantPos]
		fi.close()
		
	print ("relevant pos", len(relevantPos))				

	#Get average for monitoring positions
	for pos in monitoringPos:
		vals = monitoringPos[pos]
		monitoringPos[pos] = sum(vals) / len(vals)

	#Initialize latent values (that we need the solver to find)
	cursor = 0
	linearM = {}
	linearC = {}
	for ts in relevantHours:
		linearM[ts] = cursor
		cursor += 1

	for ts in relevantHours:
		linearC[ts] = cursor
		cursor += 1

	no2vals = {}
	for pos in relevantPos:
		if pos not in monitoringPos:
			no2vals[pos] = cursor
			cursor += 1

	x0 = np.zeros((cursor,))

	for ts in relevantHours:
		x0[linearM[ts]] = 1.0
		x0[linearC[ts]] = 0.0

	for pos in relevantPos:
		if pos not in monitoringPos:
			x0[no2vals[pos]] = 15.0

	result = opt.least_squares(EvalModel, x0, args=(withNearPos, monitoringPos, linearM, linearC, no2vals),
		verbose = 2)

	fi = gzip.open("result.dat.gz", "wb")
	fi.write(msgpack.packb((list(result.x))))
	fi.close()
	
		#a = FilterWithinDistance(withNearPos, pos, 50.0)
		#print (siteId, len(a))
		#bestDist, _ = FilterNearestToPos(inside, pos)
		#print (siteId, bestDist)

		#plt.annotate(siteId, # this is the text
		#	         pos, # this is the point to label
		#	         textcoords="offset points", # how to position the text
		#	         xytext=(0,10), # distance from text to points (x,y)
		#	         ha='center') # horizontal alignment can be left, right or center
		
	#plt.plot(monitoringLon, monitoringLat, '.')
	#plt.show()


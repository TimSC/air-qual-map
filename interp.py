import msgpack
import gzip
from matplotlib import pyplot as plt
import numpy as np

if __name__=="__main__":

	if 1:
		print ("Reading measures")
		fi = gzip.open("measures.dat.gz", "rb")
		measures = msgpack.unpackb(fi.read(), strict_map_key=False)
		fi.close()	

	print ("Reading positions")
	fi = gzip.open("positions.dat.gz", "rb")
	positions = msgpack.unpackb(fi.read(), strict_map_key=False)
	fi.close()	

	positionsTs = np.array(list(positions.keys()))
	np.sort(positionsTs)

	thres = 60.0 #sec
	withNearPos = {}
	nearPosLi = []
	for ts in measures:
		ind = np.searchsorted(positionsTs, ts)
		if ind == 0 or ind >= len(positionsTs): continue
		segStart, segEnd = positionsTs[ind-1], positionsTs[ind]
				
		ds = ts - segStart
		de = segEnd - ts
		if abs(ds) < thres and abs(de) < thres:
			segStartPos = positions[segStart]
			segEndPos = positions[segEnd]
			frac = (ts - segStart) / (segEnd - segStart)
			oneMFrac = 1.0 - frac
			interpPos = (frac * segEndPos[0] + oneMFrac * segStartPos[0], 
				frac * segEndPos[1] + oneMFrac * segStartPos[1])

			#print (ts, ind, ds, de, interpPos)

			withNearPos[ts] = (interpPos, measures[ts])
			nearPosLi.append(interpPos)
	
	print (len(withNearPos))

	#plt.plot([p[1] for p in nearPosLi], [p[0] for p in nearPosLi])	
	#plt.show()

	fi = gzip.open("measurespos.dat.gz", "wb")
	fi.write(msgpack.packb(withNearPos))
	fi.close()


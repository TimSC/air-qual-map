import csv
import os
import msgpack
import gzip

def ReadPositions(fi):

	out = {}
	data = csv.DictReader(open(fi, "rt"))
	for d in data:
		out[int(d['timestamp'])] = (float(d['latitude']), float(d['longitude']))
	return out

def CleanDictVals(d):
	out = {}
	for k in d:
		v = d[k]
		if v == "":
			out[k] = None
		else:
			out[k] = float(v)
	return out

def ReadMeasures(fi):

	out = {}
	data = csv.DictReader(open(fi, "rt"))
	for d in data:
		ts = int(d['timestamp'])
		del d['date (UTC)']
		del d['timestamp']
		d = CleanDictVals(d)
		out[ts] = d
	return out

if __name__=="__main__":

	positions = {}
	measures = {}
	path = "/home/tim/dev/air-qual-map/portsmouth/flow/user_640957_1590655203"
	for f in os.listdir(path):
		
		c1 = "user_measures_"
		if f[:len(c1)] == c1:
			print ("Read", f)
			out = ReadMeasures(os.path.join(path, f))
			measures.update(out)
	
		c2 = "user_positions_"
		if f[:len(c2)] == c2:
			print ("Read", f)
			out = ReadPositions(os.path.join(path, f))
			positions.update(out)

	fi = gzip.open("measures.dat.gz", "wb")
	fi.write(msgpack.packb(measures))
	fi.close()	

	fi = gzip.open("positions.dat.gz", "wb")
	fi.write(msgpack.packb(positions))
	fi.close()

	shared = set(measures.keys()).intersection(positions.keys())
	print ("shared timestamps", len(shared))


Scripts to analyse air quality data in Portsmouth, UK based on PCC measurements and Flow data.

* parse.py Convert raw csv inputs downloaded from plume labs into something faster to process (using msgpack)
* interp.py The timestamps of AQ measurements don't usually align with position measurements. This script takes output of parse.py and finds position of each measurement.

